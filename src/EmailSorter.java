import java.util.HashMap;
import java.util.Map;

import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.PropertySet;
import microsoft.exchange.webservices.data.core.enumeration.property.BasePropertySet;
import microsoft.exchange.webservices.data.core.enumeration.property.WellKnownFolderName;
import microsoft.exchange.webservices.data.core.enumeration.search.SortDirection;
import microsoft.exchange.webservices.data.core.service.folder.Folder;
import microsoft.exchange.webservices.data.core.service.item.Item;
import microsoft.exchange.webservices.data.core.service.schema.ItemSchema;
import microsoft.exchange.webservices.data.search.FindFoldersResults;
import microsoft.exchange.webservices.data.search.FindItemsResults;
import microsoft.exchange.webservices.data.search.FolderView;
import microsoft.exchange.webservices.data.search.ItemView;

public class EmailSorter {

	private ExchangeService supportService;
	private Map<String, Item> lastEmail;
	
	public EmailSorter(ExchangeService supportService){
		this.supportService = supportService;
		lastEmail = new HashMap<String, Item>();
	}
	
	public void run() throws Exception{
		FindFoldersResults findResults = supportService.findFolders(WellKnownFolderName.MsgFolderRoot, new FolderView(Integer.MAX_VALUE));
		Folder complete = null;
		Folder pending = null;
		for (Folder folder : findResults.getFolders()) {
			if (folder.getDisplayName().equals("Complete")){
				complete = folder;
			}
			else if (folder.getDisplayName().equals("Pending")){
				pending = folder;
			}
			if(complete != null && pending != null){
				break;
			}
		}
		FindItemsResults<Item> pendingEmails = findItems(supportService, pending);
		for(Item email : pendingEmails){
			String subject = email.getSubject();
			subject = subject.replaceAll("RE:", "");
			subject = subject.replaceAll("FW:", "");
			subject = subject.trim();
			if(lastEmail.containsKey(subject)){
				Item arrayEmail = lastEmail.get(subject);
				if(arrayEmail.getDateTimeReceived().after(email.getDateTimeReceived())){
					email.move(complete.getId());
					System.out.println("test");
				}
				else{
					arrayEmail.move(complete.getId());
					lastEmail.remove(subject);
					lastEmail.put(subject, email);
				}
			}
			else{
				lastEmail.put(subject, email);
			}
		}
	}
	
    public FindItemsResults<Item> findItems(ExchangeService service, Folder folder) {
    	ItemView view = new ItemView(1000);
    	try {
	    	view.getOrderBy().add(ItemSchema.DateTimeReceived, SortDirection.Ascending);
	    	view.setPropertySet(new PropertySet(BasePropertySet.IdOnly, ItemSchema.Subject, ItemSchema.DateTimeReceived));
	
	    	FindItemsResults<Item> findResults = service.findItems(folder.getId(), view);
			if(findResults.getTotalCount() < 1){
				return null;
			}
	        service.loadPropertiesForItems(findResults, PropertySet.FirstClassProperties);
	    	System.out.println("Total number of items found: " + findResults.getTotalCount());

	    	for (Item item : findResults) {
	    		System.out.println(item.getSubject());
	    		System.out.println(item.getDateTimeReceived());
	    	}
	    	return findResults;
		} 
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
    }
}
