import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ProcessSpreadsheet {
	
	private String connectionString;
	private Connection connection;   
	private Calendar cal;
	
	public ProcessSpreadsheet(Calendar cal){
		this.cal = cal;
		connectionString = "jdbc:sqlserver://epicor;database=MfgSys803;user=query;password=query;";
		try{
			connection = DriverManager.getConnection(connectionString);
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public int makePartSheet(String fileName, ArrayList<String> partList){
		XSSFWorkbook wb = new XSSFWorkbook();
		ArrayList<String> headers = new ArrayList<String>(Arrays.asList("Job #", "Part #", "Part Description", "Quantity", "Date"));
		Sheet sheet = prepWorkbookPage(wb, fileName, headers);
		String dateString = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		int rowC = 1;
		try{
			Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			StringBuilder querySql = new StringBuilder("SELECT jobnum, partnum, createdate, partdescription, prodqty FROM dbo.jobhead WHERE ");
			for(int x = 0; x < partList.size(); x++){
				String partNum = partList.get(x);
				if (partNum.matches("[0-9]+") && partNum.length() == 5) {
					if(x == 0){
						querySql.append("createdate = \'").append(dateString).append("\' AND (");
					}
					querySql.append("partnum = \'").append(partNum).append("\'");
					if(x != partList.size() - 1){
						querySql.append(" OR ");
					}
					else{
						querySql.append(" ) ");
					}
				}
			}
			System.out.println(querySql);
			ResultSet resultSet = statement.executeQuery(querySql.toString());
			while(resultSet.next()){
				System.out.println(rowC);
				Row row = sheet.createRow(rowC);
				row.createCell(0).setCellValue(resultSet.getString("jobnum"));
				row.createCell(1).setCellValue(resultSet.getString("partnum"));
				row.createCell(2).setCellValue(resultSet.getString("partdescription"));
				row.createCell(3).setCellValue(resultSet.getString("prodqty"));
				row.createCell(4).setCellValue(resultSet.getString("createdate"));
				rowC++;
			}
			finishSheet(sheet, headers.size());
			try{
				FileOutputStream fileOut = new FileOutputStream(fileName);
			    wb.write(fileOut);
			    fileOut.close();
				wb.close();
				System.out.println("Finished Saving File");
			}
			catch(IOException e){
				e.printStackTrace();
			}
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return rowC;
	}
	
	public int makeSheet(String fileName){
		XSSFWorkbook wb = new XSSFWorkbook();
		ArrayList<String> headers = new ArrayList<String>(Arrays.asList("PO Number","Vendor", "PO Line", "Purchaser", "Part Number", "Line Description", "Cost", "Quantity", "Total", "Daily Total"));
		Sheet sheet = prepWorkbookPage(wb, fileName, headers);
		String dateString = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		int rowC = 1;
		try{
			Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			String querySql = "SELECT poheader.ponum, podetail.poline, voidorder, vendor.name, buyerid, partnum, linedesc, unitcost, orderqty, orderdate FROM poheader" 
				+ " INNER JOIN podetail ON poheader.ponum = podetail.ponum"
				+ " INNER JOIN vendor ON poheader.vendornum = vendor.vendornum" 
				+ " WHERE orderdate = \'" + dateString + "\' AND voidorder != 1 AND voidline != 1"
				+ " ORDER BY poheader.ponum, poline";
			ResultSet resultSet = statement.executeQuery(querySql);
			System.out.println(querySql);
			while(resultSet.next()){
				System.out.println(rowC);
				Row row = sheet.createRow(rowC);
				row.createCell(0).setCellValue(resultSet.getInt("ponum"));
				row.createCell(1).setCellValue(resultSet.getString("name"));
				row.createCell(2).setCellValue(resultSet.getInt("poline"));
				row.createCell(3).setCellValue(resultSet.getString("buyerid"));
				row.createCell(4).setCellValue(resultSet.getString("partnum"));
				row.createCell(5).setCellValue(resultSet.getString("linedesc"));
				double cost = resultSet.getDouble("unitcost");
				int quantity = resultSet.getInt("orderqty");
				row.createCell(6).setCellValue(cost);
				row.createCell(7).setCellValue(quantity);
				row.createCell(8).setCellFormula("PRODUCT(G" + (rowC + 1) + ",H" + (rowC + 1) + ")");
				rowC++;
			}
			if(sheet.getRow(1) != null){
				Cell total = sheet.getRow(1).createCell(9);
				total.setCellFormula("SUM(I2:I" + (rowC + 1) + ")");
			}
			finishSheet(sheet, headers.size());
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		try{
			FileOutputStream fileOut = new FileOutputStream(fileName);
		    wb.write(fileOut);
		    fileOut.close();
			wb.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
		return rowC;
	}
	public Sheet prepWorkbookPage(XSSFWorkbook wb, String name, ArrayList<String> headers){
	    CreationHelper createHelper = wb.getCreationHelper();
	    Sheet sheet = wb.createSheet(name.replaceAll("[^A-Za-z0-9 ]", "").toLowerCase());
	    Row row = sheet.createRow((short)0);
	    CellStyle style= wb.createCellStyle();
	    XSSFFont defaultFont= wb.createFont();
	    defaultFont.setFontHeightInPoints((short)10);
	    defaultFont.setFontName("Arial");
	    defaultFont.setColor(IndexedColors.BLACK.getIndex());
	    defaultFont.setBold(false);
	    defaultFont.setItalic(false);

	    XSSFFont font= wb.createFont();
	    font.setFontHeightInPoints((short)10);
	    font.setFontName("Arial");
	    font.setColor(IndexedColors.BLACK.getIndex());
	    font.setBold(true);
	    font.setItalic(false);
	    
	    style.setFont(font);
	    row.setRowStyle(style);
	    
	    Cell tCell = null;
	    for (int i = 0; i < headers.size(); i++){
	    	tCell = row.createCell(i);
	    	tCell.setCellValue(createHelper.createRichTextString(headers.get(i)));
		    tCell.setCellStyle(style);
	    }
	    return sheet;
	}
	
	public void finishSheet(Sheet sheet, int columnCount){
	    for (int i = 0; i < columnCount; i++){
	    	sheet.autoSizeColumn(i);
	    }
	}
}
