import java.io.File;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;

import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.service.item.EmailMessage;
import microsoft.exchange.webservices.data.property.complex.MessageBody;

public class AutoEmails {
	
	private String user;
	private boolean testMode;
	private Calendar cal;
	private ExchangeService service;

	public AutoEmails(String user, boolean testMode, Calendar cal, ExchangeService service){
		this.user = user;
		this.testMode = testMode; 
		this.cal = cal;
		this.service = service;
	}
	
	public void sendPartEmail(){
		ArrayList<String> partNums = new ArrayList<String>();
		File partList = new File("\\\\vrdata1\\Production\\mssPartListForEmail.txt");
		Scanner inputStream;
		try {
			inputStream = new Scanner(partList);
			while(inputStream.hasNext()){
				partNums.add(inputStream.nextLine());
			}
			inputStream.close();
		} 
		catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
		String fileName = "New_MSS_Builds_" + dateFormat.format(cal.getTime()) + ".xlsx";
		ProcessSpreadsheet ps = new ProcessSpreadsheet(cal);
		int rows = ps.makePartSheet(fileName, partNums);
		
		try {
			EmailMessage message = new EmailMessage(service);
			if(!testMode){
			    message.getToRecipients().add("kyle.mccarraher@videoray.com");
			}
			message.getToRecipients().add(user);
			if(rows > 1){
				message.setSubject("New MSS Builds " + dateFormat.format(cal.getTime()));
				message.setBody(MessageBody.getMessageBodyFromText("Attached are the new MSS builds for " + dateFormat.format(cal.getTime())));
				message.getAttachments().addFileAttachment(fileName);
			}
			else{
				message.setSubject("No MSS Builds " + dateFormat.format(cal.getTime()));
				message.setBody(MessageBody.getMessageBodyFromText("No new MSS parts were built for " + dateFormat.format(cal.getTime())));
			}
			message.send();
		} 
		catch (Exception e1) {
			e1.printStackTrace();
		}
    }
			    
	public void sendEmail(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
		String fileName = "New_Epicor_Orders_" + dateFormat.format(cal.getTime()) + ".xlsx";
		ProcessSpreadsheet ps = new ProcessSpreadsheet(cal);
		int rows = ps.makeSheet(fileName);
		
		try {
			EmailMessage message = new EmailMessage(service);
			if(!testMode){
			    message.getToRecipients().add("erik.nadeau@videoray.com");
			    message.getToRecipients().add("nancy.steiginga@videoray.com");
			    message.getToRecipients().add("tom.bloom@videoray.com");
			    message.getToRecipients().add("lori.endlich@videoray.com");
			}
			message.getToRecipients().add(user);
			if(rows > 1){
				message.setSubject("New Epicor Orders " + dateFormat.format(cal.getTime()));
				message.setBody(MessageBody.getMessageBodyFromText("Attached are the epicor orders placed for " + dateFormat.format(cal.getTime())));
				message.getAttachments().addFileAttachment(fileName);
			}
			else{
				message.setSubject("No Epicor Orders For " + dateFormat.format(cal.getTime()));
				message.setBody(MessageBody.getMessageBodyFromText("No new orders were placed in Epicor on " + dateFormat.format(cal.getTime())));
			}
			message.send();
		} 
		catch (Exception e1) {
			e1.printStackTrace();
		}
	}
}
