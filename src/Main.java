import java.io.File;
import java.io.FileNotFoundException;
import java.net.URI;
import java.util.Calendar;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.BasicConfigurator;

import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion;
import microsoft.exchange.webservices.data.credential.ExchangeCredentials;
import microsoft.exchange.webservices.data.credential.WebCredentials;

public class Main {
	
	private static boolean testMode = false;
	private static String user;
	private static String pass;
	private static String supportUser;
	private static String supportPass;
	private static ExchangeService service;
	private static ExchangeService supportService;

    public static void main(String[] args) {
    	BasicConfigurator.configure();
    	Runtime.getRuntime().addShutdownHook(new Thread() {
    		public void run() {
    			closeServices();
    		}
    	});
    	try{
    		File credentialsFile = new File("credentials.txt");
    		Scanner inputStream = new Scanner(credentialsFile);
    		user = inputStream.nextLine();
        	pass = inputStream.nextLine();
        	supportUser = inputStream.nextLine();
        	supportPass = inputStream.nextLine();
        	inputStream.close();
    	}
    	catch(FileNotFoundException e){
    		e.printStackTrace();
    	}
    	boolean sentToday = false;
    	service = new ExchangeService(ExchangeVersion.Exchange2010_SP2);
    	ExchangeCredentials credentials = new WebCredentials(user, pass);
    	service.setCredentials(credentials);
    	
    	supportService = new ExchangeService(ExchangeVersion.Exchange2010_SP2);
    	ExchangeCredentials supportCredentials = new WebCredentials(supportUser, supportPass);
    	supportService.setCredentials(supportCredentials);
    	try {
			service.setUrl(new URI("https://outlook.office365.com/EWS/Exchange.asmx"));
			supportService.setUrl(new URI("https://outlook.office365.com/EWS/Exchange.asmx"));
		} 
    	catch (Exception e1) {
			e1.printStackTrace();
		}
    	while(true){
    		try {
		        Calendar cal = Calendar.getInstance();
		        System.out.println(cal.get(Calendar.HOUR_OF_DAY));
				if((cal.get(Calendar.HOUR_OF_DAY) == 23 && !sentToday) || testMode){
					AutoEmails autoEmails = new AutoEmails(user, testMode, cal, service);
					autoEmails.sendEmail();
					autoEmails.sendPartEmail();
					new EmailSorter(supportService).run();
					sentToday = true;
				}
				if(cal.get(Calendar.HOUR_OF_DAY) == 1){
					sentToday = false;
				}
				TimeUnit.SECONDS.sleep(600);
			} 
    		catch (Exception e) {
				e.printStackTrace();
			}
    	}
    }
    public static void closeServices(){
    	System.out.println("Closing connections...");
    	service.close();
		supportService.close();
    }
}